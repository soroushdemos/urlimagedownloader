# URL Image Downloader

A simple Kotlin application that given a website:

 - Detects images on the website (by injecting a js into WebView)
- Shows a BottomSheet indicating the number of pictures; Call to Action to Downlaod
- User can select/download all images

![demo](demo.gif)
