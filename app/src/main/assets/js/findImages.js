if (AndroidImageDownloader__findImageUrls == undefined) {
    /**
     * DO NOT USE // STYLE COMMENTING BECAUSE NEW-LINE CHARS GET REMOVED BY HOST APPLICATION
     */
    function AndroidImageDownloader__findImageUrls() {
        /**
         * Gotta use var instead of let otherwise won't work on Marshmallow
         */
        var Android = AndroidImageDownloader__Interface;
        var isValidUrl=function(url){return (url.includes && url.includes("http://") || url.includes("https://"))};
        var images = document.getElementsByTagName("img");
        var urls = [];
        for (var i = 0; i < images.length; i++) {
            var src = images[i].src;
            if (isValidUrl(src)) urls.push(src);
        }
        Android.setFoundImageUrls(urls);
    }
    AndroidImageDownloader__findImageUrls();
}