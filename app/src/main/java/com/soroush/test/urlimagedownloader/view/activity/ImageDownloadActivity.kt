package com.soroush.test.urlimagedownloader.view.activity

import android.app.Activity
import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import com.soroush.test.urlimagedownloader.R
import com.soroush.test.urlimagedownloader.helper.DownloadHelper
import com.soroush.test.urlimagedownloader.helper.Permissions
import com.soroush.test.urlimagedownloader.helper.webview.Tools
import com.soroush.test.urlimagedownloader.view.adapter.ImageRecyclerViewAdapter
import com.soroush.test.urlimagedownloader.viewmodel.ImageDownloadViewModel
import kotlinx.android.synthetic.main.activity_image_download.*

class ImageDownloadActivity : AppCompatActivity() {
    private val viewModel = ImageDownloadViewModel()
    private val imageRecyclerViewAdapter = ImageRecyclerViewAdapter()

    /*****************************
     * AppCompatActivity Methods *
     *****************************/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_download)
        setupViews(intent.extras)
    }

    override fun onStart() {
        super.onStart()
        Permissions.checkWriteExternal(this)
        viewModel.setDownloadHelper(DownloadHelper(
            getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        ))
        Tools.checkIfWifiConnected(this) > 0
    }

    /******************
     * Helper Methods *
     ******************/
    private fun setupViews(bundle: Bundle?) {
        recyclerView_images.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(
                context,
                if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) 5 else 3
            )
            adapter = imageRecyclerViewAdapter
        }
        bundle?.getStringArray(IMAGE_URLS_ARG)?.let {
            viewModel.setImageList(it)
        }

        viewModel.getImageList().observe(this, Observer {
            imageRecyclerViewAdapter.setImageDownloadStatuses(it)
        })

        fab_download.setOnClickListener {
            viewModel.startDownload(imageRecyclerViewAdapter.getSelectedUrls())
        }
    }

    /**********
     * Static *
     **********/
    companion object {
        private const val IMAGE_URLS_ARG = "image_urls_arg"

        fun start(activity: Activity, imageUrls: Array<String>) =
            Intent(activity, ImageDownloadActivity::class.java)
                .apply {
                    val bundle = Bundle()

                    bundle.putStringArray(IMAGE_URLS_ARG, imageUrls)
                    putExtras(bundle)
                    activity.startActivity(this)
                }
    }
}