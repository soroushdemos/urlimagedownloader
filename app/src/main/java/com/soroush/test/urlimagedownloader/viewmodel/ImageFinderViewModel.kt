package com.soroush.test.urlimagedownloader.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ImageFinderViewModel : ViewModel() {
    private var url: MutableLiveData<String> = MutableLiveData()

    init {
        initializeLiveData()
    }

    fun getUrl(): LiveData<String> = this.url

    fun setUrl(url: String) {
        this.url.value = url
    }

    /*******************
     * Helper Methods *
     *******************/
    private fun initializeLiveData() {
        url.value = "http://images.google.com"
    }
}