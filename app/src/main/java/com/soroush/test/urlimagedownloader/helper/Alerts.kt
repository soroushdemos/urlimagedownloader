package com.soroush.test.urlimagedownloader.helper

import android.app.AlertDialog
import android.content.Context
import com.soroush.test.urlimagedownloader.R

object Alerts {
    fun showAlertDialogWithOk(context: Context, msg: String) {
        AlertDialog.Builder(context).apply {
            setMessage(msg)
                .setPositiveButton(R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
            create()
            show()
        }
    }
}