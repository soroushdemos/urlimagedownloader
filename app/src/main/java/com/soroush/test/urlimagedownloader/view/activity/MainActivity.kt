package com.soroush.test.urlimagedownloader.view.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.PersistableBundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.webkit.JavascriptInterface
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.soroush.test.urlimagedownloader.R
import com.soroush.test.urlimagedownloader.helper.Constants
import com.soroush.test.urlimagedownloader.helper.Permissions
import com.soroush.test.urlimagedownloader.helper.webview.Tools
import com.soroush.test.urlimagedownloader.helper.webview.WebViewClient
import com.soroush.test.urlimagedownloader.helper.webview.WebViewJavascriptInterface
import com.soroush.test.urlimagedownloader.view.fragment.CallToActionFragment
import com.soroush.test.urlimagedownloader.view.fragment.CallToActionListener
import com.soroush.test.urlimagedownloader.viewmodel.ImageFinderViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity(),
    CallToActionListener {
    private val viewModel = ImageFinderViewModel()
    private val webViewClient = WebViewClient(this)
    private var urlValues: Array<String> = arrayOf()

    private val javaScriptInterface = object : WebViewJavascriptInterface() {
        @JavascriptInterface
        override fun setFoundImageUrls(urls: Array<String>) {
            if (urls.isNotEmpty()) {
                promptImagePreviews(urls)
                urlValues = urls
            }
        }

        @JavascriptInterface
        override fun log(string: String) {}
    }

    /******************************
     * AppCompatActivity Methods *
     ******************************/
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        setupViews()
        setViewCallbacks()
    }

    override fun onStart() {
        super.onStart()
        // Here, thisActivity is the current activity
        Permissions.checkInternet(this)
        setViewModelObservers()
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle) {
        super.onSaveInstanceState(outState, outPersistentState)
        webView.saveState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        webView.restoreState(savedInstanceState)
    }

    override fun onBackPressed() {
        if (webView.canGoBack()) webView.goBack()
        else super.onBackPressed()
    }

    /*********************************
     * CallToActionListener Methods *
     *********************************/
    override fun positiveButtonClicked() {
        ImageDownloadActivity.start(this, urlValues)
    }

    /*******************
     * Helper Methods *
     *******************/
    private fun setupViews() {
        setupWebView()
        fab_search.setOnClickListener {
            Tools.runJavaScript(webView, "${Constants.JS_FIND_IMAGES_FUNCTION_NAME}();")
        }
    }

    @SuppressLint("SetJavaScriptEnabled", "AddJavascriptInterface")
    private fun setupWebView() {
        webView.settings.javaScriptEnabled = true
        webView.addJavascriptInterface(javaScriptInterface, Constants.JS_INTERFACE_NAME)
        webView.webViewClient = webViewClient
    }

    private fun setViewCallbacks() {
        // editText
        editText_url.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE ||
                (event.action == KeyEvent.ACTION_DOWN &&
                        event.keyCode == KeyEvent.KEYCODE_ENTER)
            ) {
                viewModel.setUrl(Tools.correctUrl(v.text.toString()))
                true
            } else false
        }
    }

    private fun setViewModelObservers() {
        viewModel.getUrl().observe(this, androidx.lifecycle.Observer {

            setWebViewUrl(it)
        })

        webViewClient.viewModel.getIsLoading().observe(this, Observer {
            layout_loading.visibility = if (it) View.VISIBLE else View.GONE
        })

        webViewClient.viewModel.getUrl().observe(this, Observer {
            it?.let {
                viewModel.setUrl(it)
                editText_url.setText(it)
            }
        })
    }

    private fun setWebViewUrl(url: String) = webView.loadUrl(url)

    private fun promptImagePreviews(urls: Array<String>) {
        CallToActionFragment.show(
            fm = supportFragmentManager,
            title = resources.getString(
                if (urls.size > 1)
                    R.string.images_found_cta_title_plural
                else
                    R.string.images_found_cta_title
            )
                .replace("%s", urls.size.toString()),
            description = resources.getString(R.string.images_found_cta_desription),
            positiveText = resources.getString(R.string.yes)
        )
    }

    /**********
     * Static *
     **********/
    companion object {
        private const val MY_PERMISSIONS_REQUEST_INTERNET = 1
    }
}
