package com.soroush.test.urlimagedownloader.view.fragment

interface CallToActionListener {
    fun positiveButtonClicked() {

    }

    fun negativeButtonClicked() {

    }
}