package com.soroush.test.urlimagedownloader.view.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.soroush.test.urlimagedownloader.R
import kotlinx.android.synthetic.main.cta_bottom_sheet_fragment.*

class CallToActionFragment : BottomSheetDialogFragment() {
    private var callbackListener: CallToActionListener? = null

    /********************
     * Fragment Methods *
     ********************/
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.cta_bottom_sheet_fragment, container, false)

    override fun onStart() {
        super.onStart()
        setupViewsFromArgs(arguments)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is CallToActionListener) callbackListener = context
    }

    override fun onDetach() {
        super.onDetach()
        callbackListener = null
    }

    /******************
     * Helper Methods *
     ******************/
    private fun setupViewsFromArgs(bundle: Bundle?) {
        val textOK = requireContext().resources.getString(R.string.ok)
        bundle?.let {
            // title
            textView_title.text = it.getString(TITLE_ARGS)
            // description
            textView_description.apply {
                it.getString(DESCRIPTION_ARGS)?.let {
                    text = it
                    visibility = View.VISIBLE
                } ?: run { visibility = View.GONE }
            }
            // positive button
            button_positive.apply {
                text = it.getString(POSTIVIE_TEXT_ARGS) ?: textOK
                setOnClickListener {
                    callbackListener?.positiveButtonClicked()
                }
            }
            // negative button
            button_negative.apply {
                it.getString(NEGATIVE_TEXT_ARGS)?.let {
                    text = it
                    visibility = View.VISIBLE
                } ?: run {
                    visibility = View.GONE
                }
                setOnClickListener {
                    callbackListener?.negativeButtonClicked()
                }
            }
        }
    }

    /**********
     * Static *
     **********/
    companion object {
        private val TAG = CallToActionFragment::class.java.canonicalName
        private const val TITLE_ARGS = "title_args"
        private const val DESCRIPTION_ARGS = "description_args"
        private const val POSTIVIE_TEXT_ARGS = "positive_text_args"
        private const val NEGATIVE_TEXT_ARGS = "negative_text_args"

        fun show(
            fm: FragmentManager,
            title: String,
            description: String? = null,
            positiveText: String? = null,
            negativeText: String? = null
        ) {
            
            var fragment = fm.findFragmentByTag(TAG) as CallToActionFragment?
            if (fragment != null) fm.beginTransaction().remove(fragment).commit()
            fragment =
                CallToActionFragment()
            val bundle = Bundle()
            bundle.putString(TITLE_ARGS, title)
            if (description != null) bundle.putString(DESCRIPTION_ARGS, description)
            if (positiveText != null) bundle.putString(POSTIVIE_TEXT_ARGS, positiveText)
            if (negativeText != null) bundle.putString(POSTIVIE_TEXT_ARGS, negativeText)
            fragment.arguments = bundle
            fragment.show(
                fm,
                TAG
            )
        }
    }
}
