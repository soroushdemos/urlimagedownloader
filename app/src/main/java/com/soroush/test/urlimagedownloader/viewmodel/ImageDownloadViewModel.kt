package com.soroush.test.urlimagedownloader.viewmodel

import android.app.DownloadManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.soroush.test.urlimagedownloader.helper.Constants
import com.soroush.test.urlimagedownloader.helper.DownloadHelper
import com.soroush.test.urlimagedownloader.helper.DownloadStatus
import com.soroush.test.urlimagedownloader.model.ImageDownloadStatus
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ImageDownloadViewModel : ViewModel() {
    private val imageList: MutableLiveData<List<ImageDownloadStatus>> = MutableLiveData()
    private lateinit var downloadHelper: DownloadHelper

    init {
        imageList.value = listOf()
    }

    fun getImageList(): LiveData<List<ImageDownloadStatus>> = this.imageList

    fun setImageList(imageList: Array<String>) {
        this.imageList.value = imageList.map {
            ImageDownloadStatus(
                imageUrl = it,
                downloadStatus = 0,
                downloadProgress = 0
            )
        }
    }

    fun startDownload(urls: Array<String>) {
        updateDownloadStatuses(urls.map {
            ImageDownloadStatus(it, 0, DownloadManager.STATUS_PENDING)
        })

        val chunkSize = Constants.IMAGE_DOWNLOAD_CHUNK_SIZE

        CoroutineScope(IO).launch {
            var downloadedCount = 0
            var chunkStartIndex = 0
            while (downloadedCount < urls.size) {
                var interator_index = 0
                val requestIds = mutableMapOf<Long, String>()

                // schedule a chunk of downloads
                while (chunkStartIndex + interator_index < urls.size && interator_index < chunkSize) {
                    val url = urls[chunkStartIndex + interator_index]
                    requestIds[downloadHelper.enqueDownload(url)] = url
                    interator_index++
                }

                // UI THREAD: update the downlod status to RUNNING
                withContext(Main) {
                    updateDownloadStatuses(requestIds.map {
                        ImageDownloadStatus(
                            imageUrl = it.value,
                            downloadProgress = 0,
                            downloadStatus = DownloadManager.STATUS_RUNNING
                        )
                    })
                }

                // query the status of the downloads till completed
                var completed = false
                while (!completed) {
                    requestIds
                        .map {
                            try {
                                val status = downloadHelper.checkProgreas(it.key)
                                Pair(it.value, status)
                            }catch (e: Exception) {
                                Pair(it.value, DownloadStatus(
                                    percentage = 0,
                                    status = DownloadManager.STATUS_RUNNING
                                )
                                )
                            }
                        }
                        .also {
                            // UI THREAD
                            withContext(Main) {
                                updateDownloadStatuses(it.map {
                                    ImageDownloadStatus(
                                        imageUrl = it.first,
                                        downloadProgress = it.second.percentage,
                                        downloadStatus = it.second.status
                                    )
                                })
                            }
                        }
                        .also { statuses ->
                            completed = statuses.all { it.second.percentage == 100 }
                        }
                }
                chunkStartIndex += interator_index
                downloadedCount += chunkSize
            }
        }
    }

    fun setDownloadHelper(downloadHelper: DownloadHelper) {
        this.downloadHelper = downloadHelper
    }

    /**********
     * Helper
     **********/
    private fun updateDownloadStatuses(statusList: List<ImageDownloadStatus>) {
        val updatedUrls = statusList.map { it.imageUrl }
        imageList.value = imageList.value?.map { value ->
            if (updatedUrls.contains(value.imageUrl)) {
                statusList.find { it.imageUrl == value.imageUrl }?.downloadStatus?.let {

                    value.downloadStatus = it
                }
                statusList.find { it.imageUrl == value.imageUrl }?.downloadProgress?.let {

                    value.downloadProgress = it
                }
            }
            value
        }
    }
}
