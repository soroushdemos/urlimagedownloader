package com.soroush.test.urlimagedownloader.helper.webview

import android.webkit.JavascriptInterface


abstract class WebViewJavascriptInterface {
    @JavascriptInterface
    abstract fun log(string: String)

    @JavascriptInterface
    abstract fun setFoundImageUrls(urls: Array<String>)
}