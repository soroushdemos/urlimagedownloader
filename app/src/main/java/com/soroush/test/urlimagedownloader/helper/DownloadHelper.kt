package com.soroush.test.urlimagedownloader.helper

import android.app.DownloadManager
import android.net.Uri
import android.os.Environment
import android.util.Log

class DownloadHelper constructor(private val downloadManager: DownloadManager) {
    /**
     * Inspired by: https://github.com/Ansh1234/RxDownloader
     * Seems to return only 0 and 100 percents but that might be due to small size of images
     */
    fun checkProgreas(requestId: Long): DownloadStatus {
        val query = DownloadManager.Query().apply {
            setFilterById(requestId)
        }
        val cursor = downloadManager.query(query)
        if (cursor == null || !cursor.moveToFirst()) {
            return DownloadStatus(0, DownloadManager.STATUS_PENDING)
        }
        //Get the download percent
        val bytesDownloaded = cursor.getInt(
            cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR)
        )
        val bytesTotal = cursor.getInt(
            cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES)
        )
        val percent = bytesDownloaded * 100 / bytesTotal
        //Get the download status
        val downloadStatus = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS))
        if (downloadStatus == DownloadManager.STATUS_FAILED) Log.w("sometag", "fail!")
        return DownloadStatus(percent, downloadStatus)
    }

    fun enqueDownload(url: String): Long {
        DownloadManager.Request(Uri.parse(url))
            .apply {
                setDestinationInExternalPublicDir(
                    Environment.DIRECTORY_PICTURES,
                    "${System.currentTimeMillis()}"
                )
            }
            .also {
                return downloadManager.enqueue(it)
            }
    }
}

class DownloadStatus constructor(var percentage: Int, var status: Int)