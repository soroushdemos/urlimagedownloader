package com.soroush.test.urlimagedownloader.helper.webview

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.webkit.WebView
import com.soroush.test.urlimagedownloader.R
import com.soroush.test.urlimagedownloader.helper.Alerts
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader


object Tools {
    fun correctUrl(url: String): String {
        val _url = url.trim()
        return if (!hasHttp(_url))
            "http://$_url"
        else
            _url
    }

    fun checkIfWifiConnected(context: Context): Int {
        //TODO fix deprecations
        var status = -1
        (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).apply {
            val wifi: NetworkInfo = getNetworkInfo(ConnectivityManager.TYPE_WIFI)
            val mobile: NetworkInfo = getNetworkInfo(ConnectivityManager.TYPE_MOBILE)

            if (wifi.isConnected) status = ConnectivityManager.TYPE_WIFI
            else if (mobile.isConnected) status = ConnectivityManager.TYPE_MOBILE
        }

        when (status) {
            ConnectivityManager.TYPE_WIFI -> return status
            ConnectivityManager.TYPE_MOBILE -> Alerts.showAlertDialogWithOk(
                context,
                context.resources.getString(R.string.data_only)
            )
            else -> Alerts.showAlertDialogWithOk(
                context,
                context.resources.getString(R.string.no_internet)
            )
        }
        return status
    }

    fun fetchJavascriptAssetAsString(context: Context, fileName: String): String =
        convertStreamToString(context.assets.open("js/$fileName"))

    // sdk-aware javascript runner on webview
    fun runJavaScript(webView: WebView, js: String) {
        val _js = js.replace("\n", "").replace("\t", "")

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            webView.evaluateJavascript("javascript:$_js", null)
        } else {
            webView.loadUrl("javascript:$js")
        }
    }

    /**********
     * Helper *
     **********/
    private fun hasHttp(url: String): Boolean =
        (url.contains("http://") || url.contains("https://"))


    private fun convertStreamToString(`is`: InputStream): String {
        val reader = BufferedReader(InputStreamReader(`is`))
        val sb = StringBuilder()
        var line: String? = null
        try {
            while (reader.readLine().also { line = it } != null) {
                sb.append(line + "\n")
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                `is`.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return sb.toString()
    }
}
