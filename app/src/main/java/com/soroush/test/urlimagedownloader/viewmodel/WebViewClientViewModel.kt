package com.soroush.test.urlimagedownloader.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class WebViewClientViewModel : ViewModel() {
    private var isLoading: MutableLiveData<Boolean> = MutableLiveData()
    private var url: MutableLiveData<String> = MutableLiveData()

    init {
        isLoading.value = false
    }

    fun getIsLoading(): LiveData<Boolean> = this.isLoading

    fun setLoading(isLoading: Boolean) {
        this.isLoading.value = isLoading
    }

    fun getUrl(): LiveData<String> = this.url
    fun setUrl(url: String) {
        this.url.value = url
    }
}