package com.soroush.test.urlimagedownloader.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
/**
 * @param imageUrl
 * @param downloadStatus {@link DownloadManager.STATUS_....}
 */
data class ImageDownloadStatus(
    var imageUrl: String,
    var downloadStatus: Int,
    var downloadProgress: Int
) : Parcelable