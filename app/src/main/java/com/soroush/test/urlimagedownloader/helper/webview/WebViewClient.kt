package com.soroush.test.urlimagedownloader.helper.webview

import android.content.Context
import android.graphics.Bitmap
import android.webkit.WebView
import android.webkit.WebViewClient
import com.soroush.test.urlimagedownloader.helper.Constants
import com.soroush.test.urlimagedownloader.viewmodel.WebViewClientViewModel

class WebViewClient(private val context: Context) : WebViewClient() {
    val viewModel = WebViewClientViewModel()

    /*************************
     * WebViewClient Methods *
     *************************/
    override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
        url?.let { viewModel.setUrl(it) }
        return super.shouldOverrideUrlLoading(view, url)
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        val js = Tools.fetchJavascriptAssetAsString(context, Constants.INJECTED_JS_FILE_NAME)
        view?.let { Tools.runJavaScript(it, js) }
        viewModel.setLoading(false)
    }

    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        super.onPageStarted(view, url, favicon)
        viewModel.setLoading(true)
    }
}