package com.soroush.test.urlimagedownloader.helper

import android.Manifest
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

object Permissions {
    fun checkInternet(activity: AppCompatActivity) =
        checkPermission(activity, Manifest.permission.INTERNET)

    fun checkWriteExternal(activity: AppCompatActivity) =
        checkPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)

    /**********
     * Helper *
     **********/
    private fun checkPermission(activity: AppCompatActivity, permission: String): Boolean {
        if (ContextCompat.checkSelfPermission(activity, permission)
            != PackageManager.PERMISSION_GRANTED
        ) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
                // ignored
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(
                    activity,
                    arrayOf(permission),
                    0
                )
            }
        } else {
            // has permission
            return true
        }
        return false
    }
}