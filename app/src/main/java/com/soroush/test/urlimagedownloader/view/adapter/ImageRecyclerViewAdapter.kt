package com.soroush.test.urlimagedownloader.view.adapter

import android.app.DownloadManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.soroush.test.urlimagedownloader.R
import com.soroush.test.urlimagedownloader.model.ImageDownloadStatus
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.viewholder_image.view.*

class ImageRecyclerViewAdapter : RecyclerView.Adapter<ImageViewHolder>() {
    private var imageDownloadStatuses: List<ImageDownloadStatus> = listOf()
    private var selectedUrls: MutableList<String> = mutableListOf()
    private var selectionOn: Boolean = false

    fun setImageDownloadStatuses(urls: List<ImageDownloadStatus>) {
        imageDownloadStatuses = urls
        notifyDataSetChanged()
    }

    fun getSelectedUrls(): Array<String> =
        if (selectedUrls.size > 0)
            selectedUrls.toTypedArray()
        else imageDownloadStatuses.map {
            it.imageUrl
        }.toTypedArray()

    /********************************
     * RecyclerView.Adapter Methods *
     ********************************/
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.viewholder_image, parent, false) as CardView
        )
    }



    override fun getItemCount(): Int = imageDownloadStatuses.size

    override fun onBindViewHolder(vh: ImageViewHolder, position: Int) {
        if (imageDownloadStatuses[position].imageUrl.isNotEmpty()) {
            val status = imageDownloadStatuses[position]
            vh.itemView.apply {
                // load image
                //TODO enable disk caching so it won't keep reloading; apparently by adding custom okHttp downloader that has caching
                Picasso.get()
                    .load(status.imageUrl)
                    .placeholder(R.drawable.ic_hourglass_100dp)
                    .error(R.drawable.ic_error_red_100dp)
                    .fit()
                    .into(imageView)

                setOnLongClickListener {
                    toggleSelectionMode()
                    true
                }

                setOnClickListener { if (selectionOn) checkbox.callOnClick() }

                checkbox.apply {
                    setOnCheckedChangeListener { _, isChecked ->
                        if (isChecked)
                            selectedUrls.add(status.imageUrl)
                        else
                            selectedUrls.remove(status.imageUrl)
                    }
                    visibility = if (selectionOn) View.VISIBLE else View.GONE
                }

                //TODO fix the jank!
                progressbar_download.apply {
                    visibility = when (status.downloadStatus) {
                        DownloadManager.STATUS_PENDING, DownloadManager.STATUS_RUNNING -> {
                            View.VISIBLE
                        }
                        DownloadManager.STATUS_FAILED, DownloadManager.STATUS_SUCCESSFUL -> {
                            //TODO handle FAIL case
                            View.GONE
                        }
                        else -> {
                            View.GONE
                        }
                    }
                }

                imageView_downloadSuccess.visibility =
                    if (status.downloadProgress == 100)
                        View.VISIBLE
                    else
                        View.GONE
            }
        }
    }

    /******************
     * Helper Methods *
     ******************/
    private fun toggleSelectionMode() {
        selectionOn = !selectionOn
        if (!selectionOn) selectedUrls = mutableListOf()
        notifyDataSetChanged()
    }
}

class ImageViewHolder(itemView: CardView) : RecyclerView.ViewHolder(itemView)