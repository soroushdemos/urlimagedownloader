package com.soroush.test.urlimagedownloader.helper

class Constants {
    companion object {
        const val IMAGE_DOWNLOAD_CHUNK_SIZE = 20
        const val INJECTED_JS_FILE_NAME = "findImages.js"
        const val JS_FIND_IMAGES_FUNCTION_NAME = "AndroidImageDownloader__findImageUrls"
        const val JS_INTERFACE_NAME = "AndroidImageDownloader__Interface"
    }
}